import { User } from './user';
import { database } from '../../config/database';

export namespace UserHelper {
    export const getAllUser = (): Array<User> => {
        return database.prepare('SELECT * FROM user').all();
    };

    export const getUser = (name: string): User | undefined => {
        return database.prepare('SELECT * FROM user WHERE name = ?').get([name]);
    };
    
    export const createUser = (name: string): void => {
        database.prepare('INSERT INTO user(name) VALUES(?)').run([name]);
    };
}