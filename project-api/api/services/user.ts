import { User } from '../models/user/user';
import { UserHelper } from '../models/user/helpers';

export namespace UserService {
    export const getAllUser = (): Array<User> => {
        return UserHelper.getAllUser();
    };

    export const getUser = (name: string): User | undefined => {
        return UserHelper.getUser(name);
    };
    
    export const createUser = (name: string): void => {
        UserHelper.createUser(name);
    };
}