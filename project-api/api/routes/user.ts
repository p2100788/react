import { Request, Response } from 'express';
import { UserService } from '../services/user';

const express = require('express');
const route = express.Router();

route.get('/', async (req: Request, res: Response) => {
    const users = await UserService.getAllUser();
    res.send(users);
});

route.get('/:name', async (req: Request, res: Response) => {
    const userName = req.params.path;
    const user = await UserService.getUser(userName);
    res.send(user);
});

route.post('/create', async (req: Request, res: Response) => {
    const { name } = req.body;
    await UserService.createUser(name);
    res.sendStatus(201);
});

export default route;