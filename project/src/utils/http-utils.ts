export const API_URL = "http://localhost:8080/api/user"

export const fetchData = <T>(url: string): Promise<T> => {
    return fetch(url).then((res) => res.json());
}