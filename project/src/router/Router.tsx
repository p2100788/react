import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from '../pages/Home/Home';
import Authentication from '../pages/Authentication/Connexion/Authentication';
import Create from '../pages/Authentication/Create/Create';
import Annonce from '../pages/Annonce/Annonce/Annonce';
import MyAnnonce from '../pages/Annonce/MyAnnonce/MyAnnonce';
import Profil from '../pages/Profil/Profil';
import NavBar from '../components/NavBar/NavBar';

function Router() {
    return (
        <BrowserRouter>
            <NavBar />
            <Routes>
                <Route path={'/'} element={<Home />} />
                <Route path={'/login'} element={<Authentication />} />
                <Route path={'/sing-up'} element={<Create />} />
                <Route path={'/profil/:name'} element={<Profil />} />
                <Route path={'/annonce'} element={<Annonce />} />
                <Route path={'/:name/myannonce'} element={<MyAnnonce />} />
            </Routes>
        </BrowserRouter>
    );
}

export default Router;