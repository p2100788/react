import React, { useState } from 'react';
import '../Authentication.css';
import Button from '../../../components/Button/Button';
import { API_URL, fetchData } from '../../../utils/http-utils';
import { NavLink } from 'react-router-dom';
import { log } from 'console';

type User = {
  id: number;
  name: string;
};

function Create() {
  const [name, setName] = useState<string>('');

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.name === 'username') {
      setName(event.target.value);
    }
  };

  const handleCreateUser = () => {
    fetch(`${API_URL}/create`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ name })
    }).then(res => res.json())
      .then(res => console.log(res));
  };

  return (
    <div className="auth">
        <h1>S'INSCRIRE</h1>
      <div className="input-container">
        <label>Identifiant :</label>
        <input
          type="text"
          name="username"
          value={name}
          onChange={handleInputChange}
        />
      </div>
      <div>
        <Button name='crée un compte' clicked={handleCreateUser} />
      </div>

    <NavLink to={'/login'}>
        <p>déjà un compte ? connectez-vous ici</p>
    </NavLink>

    </div>
  );
}

export default Create;