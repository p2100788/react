import React, { useState } from 'react';
import '../Authentication.css';
import Button from '../../../components/Button/Button';
import { API_URL, fetchData } from '../../../utils/http-utils';
import { NavLink, useNavigate } from 'react-router-dom';

type User = {
  id: number;
  name: string;
};

function Authentication() {
  const [name, setName] = useState<string>('');
  const navigate = useNavigate();

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.name === 'username') {
      setName(event.target.value);
    }
  };

  const handleConnexion = () => {
    getUser(name.toLowerCase())
      .then((userData) => {
        if (userData) {
          navigate(`/${name}/myannonce`);
        } else {
          console.error(`L'utilisateur ${name} n'existe pas :(`);
        }
      })
      .catch((error) => {
        console.error(`Une erreur est survenue : ${error}`);
      });
  };

  const getUser = (name: string): Promise<User | null> => {
    return fetchData<User>(`${API_URL}?name=${name}`).catch(() => null);
  };

  return (
    <div className="auth">
      <h1>SE CONNECTER</h1>
      <div className="input-container">
        <label>Identifiant :</label>
        <input
          type="text"
          name="username"
          value={name}
          onChange={handleInputChange}
        />
      </div>
      <div>
        <Button name='Se connecter' clicked={handleConnexion} />
      </div>

      <NavLink to={'/sing-up'}>
          <p>pas de compte ? inscrivez-vous ici</p>
      </NavLink>

    </div>
  );
}

export default Authentication;