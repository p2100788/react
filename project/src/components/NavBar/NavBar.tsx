import React from 'react';
import './NavBar.css'
import { NavLink } from 'react-router-dom';

function NavBar() {
    return (
        <div className={'navigation'}>
            <div>
                <NavLink to={'/:name/annonce'}>
                    <div>Mes annonces</div>
                </NavLink>
                <NavLink to={'/annonce'}>
                    <div>Les annonces</div>
                </NavLink>
                <NavLink to={'/profil'}>
                    <div>Mon profil</div>
                </NavLink>
            </div>
            <div>
                <NavLink to={'/login'}>
                    <div>Se déconnecter</div>
                </NavLink>
            </div>
        </div>
    )
}

export default NavBar;
